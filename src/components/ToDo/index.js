import React, { Component } from 'react'

class ToDo extends Component {
    render() {
        let textColor;
        (this.props.type === 'notDone') ? 
        textColor = 'red' : (this.props.type === 'inProgress') ?
        textColor = 'yellow' : textColor = 'green'; 
        return (
            <li>
                <span style={{color:textColor, fontFamily:'sans-serif'}}>{this.props.text}</span>
            </li>
        )
    }
}

export default ToDo
