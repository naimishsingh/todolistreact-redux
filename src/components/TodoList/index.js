import React, { Component } from "react";
import TodoType from '../TodoType'
import './index.css'

class TodoList extends Component {

  render() {
    return (
      <div>
        <table>
          <tr>
            <th>Not Done</th>
            <th>In Progress</th>
            <th>Completed</th>
          </tr>
          <tr>
            <td><TodoType type='notDone' /></td>
            <td> <TodoType type='inProgress' /></td>
            <td> <TodoType type='completed' /></td>
          </tr>
        </table>
      </div>
    );
  }
}

export default TodoList;
