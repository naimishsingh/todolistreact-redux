import React, { Component } from "react";
import addTodoItem from '../../actions/addTodoItemAction'
import { connect } from 'react-redux'
import './index.css'

class AddTodoItem extends Component {
  
    constructor(props){
        super(props);
        this.state = {
            item: "",
        };
        this.setItem = this.setItem.bind(this);
    }

  setItem = (e) => {
    this.setState({
      item: e.target.value,
    });
  };

  render() {
    return (
      <div className='addTodoItem'>
        <form
          onSubmit={(e) => {
            e.preventDefault()
            this.props.addTodoItem(this.state.item);
            this.setState({
              item:''
            })
          }}
        >
          <input type='text' value={this.state.item} onChange={this.setItem} placeholder='Add your item here'/> <br />
          <button className='addTodoItemBtn' type="submit">Add Item</button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => {
    return {
        todos : state.todos
    }
}

const mapDispatchToProps = dispatch => {
    return {
      addTodoItem : item => dispatch(addTodoItem(item))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddTodoItem);
