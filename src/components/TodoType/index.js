import React, { Component } from "react";
import { connect } from 'react-redux'
import Todo from '../ToDo';
import './index.css'
import changeStatus from '../../actions/changeStatusAction'

class TodoType extends Component {

  render() {
    const { todos, type, changeStatus } = this.props;
    return (
      <div>
        <ul className='list'>
          {todos.map((todo) => (
            (todo.status === type) && 
            (
                <div className='todoType'>
                    <Todo key={todo.id} {...todo} type={type}/>
                    <button className = 'btn' onClick={() => changeStatus(todo.id, type, 0)}>{'<--'}</button>{'  '}
                    <button className='btn' onClick={() => changeStatus(todo.id, type, 1)}>{'-->'}</button>
                </div>   
            )
          ))}
        </ul>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
      todos : state.todos
  }
}

const mapDispatchToProps = dispatch => {
  return {
      changeStatus : (id, currStatus, btnId) => dispatch(changeStatus(id, currStatus, btnId))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoType);
