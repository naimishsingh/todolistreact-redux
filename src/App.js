import React, { Component } from "react";
import AddTodoItem from "./components/AddTodoItem";
import TodoList from './components/TodoList'
import { Provider } from "react-redux";
import store from "./store";

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div>
          <AddTodoItem />
          <TodoList />
        </div>
      </Provider>
    );
  }
}

export default App;
