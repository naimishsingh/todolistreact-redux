import { ADD_TODO, CHANGE_STATUS } from '../actions/type';

const initialState = {
    todos : []
}

const todoReducer = (state = initialState, action) => {
    switch(action.type){
        case ADD_TODO:
            return {
                ...state,
                todos : [...state.todos, action.todoObj]
            }
        case CHANGE_STATUS:
            let newStatus = '';
            if(action.currStatus === 'notDone'){
                (action.btnId === 1) ? newStatus = 'inProgress' : newStatus = ''
            }else if(action.currStatus === 'inProgress'){
                (action.btnId === 1) ? newStatus = 'completed' : newStatus = 'notDone'
            }else{
                (action.btnId === 1) ? newStatus = '' : newStatus = 'inProgress'
            }
            const temp = state.todos.map(todo => (
                todo.id === action.id ? {...todo, status : newStatus} : todo
            ))
            return {
                ...state,
                todos : temp
            }
        default:
            return state
    }
}

export default todoReducer