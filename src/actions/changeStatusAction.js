import { CHANGE_STATUS } from './type'

const changeStatus = (id, currStatus, btnId) => (
    {
        type : CHANGE_STATUS,
        id,
        currStatus,
        btnId
    }
)

export default changeStatus;