import { ADD_TODO } from './type';

let todoId = 0;

const addTodoItem = (item) => {
    return {
        type : ADD_TODO,
        todoObj : {
            id : todoId++,
            text : item,
            status : 'notDone'
        }
    }
}

export default addTodoItem;